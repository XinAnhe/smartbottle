
#include <SPI.h>
#include <Wire.h>
#include "WEMOS_Motor.h"
#include "logos.h"
#include "battery.h"
#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Ticker.h>
#include <AceButton.h>
#include <OneWire.h>
#include <DallasTemperature.h>

using namespace ace_button;

#define PIN_RESET 255  //
#define DC_JUMPER 0  // I2C Address: 0 - 0x3C, 1 - 0x3D

//connect to wifi
//const char* ssid = "iot_test";
//const char* password = "iotest@tuvsudcoe";
const char* ssid = "smartbottle";
const char* password = "Anhe-Xin";
const char* mqtt_server = "10.10.8.1";
String user_name = "ANLIN-BOTTLE";
String clientID ="Anlin's bottle";

WiFiClient espClient;
PubSubClient psClient(espClient);

const int BUTTON_PIN = D3;
const int ledPin = LED_BUILTIN;

// GPIO where the DS18B20 is connected to
const int oneWireBus = D4;
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);
// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);
float temperatureC = 10; 

AceButton button(BUTTON_PIN, HIGH, 0);
void handleEvent(AceButton*, uint8_t, uint8_t);

long counter = 0;
long starts = 0;
long total = 0;

//////////////////////////////////
// MicroOLED Object Declaration //
//////////////////////////////////
//#define SCREEN_WIDTH 128 // OLED display width, in pixels
//#define SCREEN_HEIGHT 64 // OLED display height, in pixels
//#define OLED_MOSI   D7 //Connect to D1 on OLED
//#define OLED_CLK    D4 //Connect to D0 on OLED 
//#define OLED_DC     D6 //Connect to DC on OLED
//#define OLED_CS     D8 //Connect to CS on OLED
//#define OLED_RESET  D5 //Connect to RES on OLED
//#define SSD1306_128_64
//Adafruit_SSD1306 oled(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

#define SSD1306_LCDWIDTH 128 // OLED display width, in pixels
#define SSD1306_LCDHEIGHT 64 // OLED display height, in pixels
#define OLED_RESET 0 // SCL - D0, SDA - D1
Adafruit_SSD1306 oled(OLED_RESET);

//Motor shiled I2C Address: 0x30
//PWM frequency: 1000Hz(1kHz)
Motor M1(0x30,_MOTOR_A, 1000);//Motor A
bool motorOn = false;

Ticker timerUploadAttr;
Ticker timerHeader;
Ticker timerMotor;

boolean iswifi = false;
int timeout = 10;

void displayHeader(){
  displayWifi();
  displahyBatteryLevel();
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  // Try to set the cursor in the middle of the screen
  oled.setCursor(29, 4);
  // Print the text:
  oled.println("Anlin (1J)");
  for (int y=18; y<=24; y++){
     for (int x=0; x<127; x++){
      oled.drawPixel(x, y, BLACK); 
     }
  } 
  oled.setCursor(0, 18);
  oled.print("Water is ");
  oled.print((float)temperatureC, 1);
  oled.println(" (C).");
  oled.display();
}

void displayWifi()
{
  if (iswifi){
    oled.drawBitmap(0, 0,  logo16_wifi_bmp, 16, 16, WHITE);
  }
  else{
    oled.drawBitmap(0, 0, logo16_Dis_wifi_bmp, 16, 16, WHITE);
  }
}

void connectWIFI() {
  delay(5);
  //  WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  int tried = 0;
  while (WiFi.status() != WL_CONNECTED && tried < timeout) {
    delay(1000);
    iswifi = false;
    Serial.print("."); 
    tried++;
  }

  if (WiFi.status() == WL_CONNECTED) {
    iswifi = true;
  }

  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP().toString());
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
}

void connectMQTT() {
 // Loop until we're reconnected or timeout
 int tried = 0;
 while (!psClient.connected() && tried < timeout) {
   Serial.print("Attempting MQTT connection...");
  
   if (psClient.connect(clientID.c_str(),user_name.c_str(),NULL)) {
      Serial.println("connected");
   } 
   else {
      Serial.print("failed, rc=");
      Serial.print(psClient.state());
      Serial.print("tried: ");
      Serial.print(tried);
      Serial.println(" try again in 1 second");
      // Wait 1 second before retrying
      delay(1000);
      tried++;
  }
 }
}

void keepMotorActive(){
  Serial.println("Trigger motor activation.");
  switchMotor(motorOn);
}

void initMotor() {
  // M1.setmotor(_CW, 90);
  // delay(5000);
  M1.setmotor(_STOP);
  Serial.println("Motor Shield Testing...");
  delay(200);
}

void initOLED() {
   // These three lines of code are all you need to initialize the
  // OLED and print the splash screen.
  
  // Before you can start using the OLED, call begin() to init
  // all of the pins and configure the OLED.
  oled.begin();
  oled.clearDisplay();
  // To actually draw anything on the display, you must call the
  // display() function. 
  
  //Note: You can change the spalsh screen by editing the array founf in the library source code
  oled.drawBitmap(0, 0, bender, 128, 64, 1);//call the drawBitmap function and pass it the array from above
  oled.display();//display the imgae 
  delay(1000);
}

void setup() {
  Serial.begin(250000);
  pinMode(BUTTON_PIN, INPUT);
  pinMode(ledPin, OUTPUT);
  // set initial state, LED off
  digitalWrite(ledPin, LOW);

  initOLED();

  displayHeader();

  // Configure the ButtonConfig with the event handler, and enable all higher
  // level events.
  ButtonConfig* buttonConfig = button.getButtonConfig();
  buttonConfig->setFeature(ButtonConfig::kFeatureClick);
  buttonConfig->setFeature(ButtonConfig::kFeatureDoubleClick);
  buttonConfig->setFeature(ButtonConfig::kFeatureLongPress);
  buttonConfig->setEventHandler(handleEvent);

  timerHeader.attach(5, displayHeader);
  timerUploadAttr.attach(60,sendAttr);
  timerMotor.attach(5, keepMotorActive);

  printText("Connecting to WiFi...");
  connectWIFI();
  psClient.setServer(mqtt_server,1883);
  connectMQTT();
  initMotor();
  printText("Press & hold button  to drink");

  // Start the DS18B20 sensor
  sensors.begin();
}

void printText(String text) {
  oled.clearDisplay();
  oled.setTextSize(0);
  oled.setTextColor(WHITE);
  // Try to set the cursor in the middle of the screen
  oled.setCursor(0, 36);
  // Print the text:
  oled.println(text);
  displayHeader();
  oled.display();
}

void switchMotor(boolean turnOn){
  motorOn = turnOn;
  if (turnOn){
    M1.setmotor(_CW, 90);
    Serial.println("MOTOR_STATUS_CW");
  }
  else{
    M1.setmotor(_STOP);
    Serial.println("MOTOR_STATUS_STOP");
  }

  delay(200);
}

void loop() {
  if (!iswifi) {
    connectWIFI();
  }
  
  if (!psClient.connected()) 
  {
      connectMQTT();
  }
  psClient.loop();

  sensors.requestTemperatures(); 
  temperatureC = sensors.getTempCByIndex(0);
//  Serial.print(temperatureC);
//  Serial.println("ºC");

  button.check();
}

void handleEvent(AceButton* /* button */, uint8_t eventType,
    uint8_t buttonState) {
  // Print out a message for all events.
  Serial.print(F("handleEvent(): eventType: "));
  Serial.print(eventType);
  Serial.print(F("; buttonState: "));
  Serial.println(buttonState);
  
  switch (eventType) {
    case AceButton::kEventLongPressed:
      Serial.println("longPress ");
      digitalWrite(ledPin, HIGH);  // LED on
      switchMotor(true);
      starts = millis();
      printText("Release to check the water intake.");
      break;
    case AceButton::kEventReleased:
      Serial.println("released");
      digitalWrite(ledPin, LOW); // LED off
      if (starts > 0){
        counter = millis() - starts;
        switchMotor(false);
        updateReading();
      }
      Serial.println("Completed release function.");
      break;
  }
}

void updateReading() {
  if (starts > 0){
    long intake = 6.875*((counter-1000)/1000);
    String msg;
    msg = "{";
    msg += "\"power\":";
    msg += String(batteryLevel);
    msg += ",";
    msg += "\"intake_time_ms\":";
    msg += String(counter);
    msg += ",";
    msg += "\"intake_temperature\":";
    msg += String(temperatureC);
    msg += ",";
    msg += "\"intake_ml\":";
    msg += String(intake); //Pump 220ml water, takes 33s. But first second without any woter out.
    msg += "}";
    psClient.publish("v1/devices/me/telemetry", msg.c_str());
    total += intake;
    msg = "Last water intake was:" + String(intake) + "ml, and total intake is: " + String(total) + "ml.";
    printText(msg);
    starts = 0;
    counter = 0;
  }
}

void sendAttr() {
  String msg_attribute;

  msg_attribute ="{";  
  msg_attribute += "\"mac\":";
  msg_attribute += "\"" + String(WiFi.macAddress())+ "\"";
        
  msg_attribute += ",";
  msg_attribute += "\"ip\":";
  msg_attribute += "\"" + WiFi.localIP().toString() + "\"";

  msg_attribute += "}";
  psClient.publish("v1/devices/me/attributes" ,msg_attribute.c_str());

}

void displahyBatteryLevel(){
  batteryLevel = getPercentage();
//  Serial.print("Current battery level: ");
//  Serial.println(batteryLevel);
  if (batteryLevel==0) {
    oled.drawBitmap(100, 0,  logo24_battery0_bmp, 24, 16, WHITE); 
  }
  else if (batteryLevel > 0 && batteryLevel <=25) {
    oled.drawBitmap(100, 0,  logo24_battery25_bmp, 24, 16, WHITE);
  }
  else if (batteryLevel>25 && batteryLevel<=50) {
    oled.drawBitmap(100, 0,  logo24_battery50_bmp, 24, 16, WHITE);
  }
  else if (batteryLevel>50 && batteryLevel<=75) {
    oled.drawBitmap(100, 0,  logo24_battery75_bmp, 24, 16, WHITE); 
  }
  else if (batteryLevel>75 && batteryLevel<=100) {
    oled.drawBitmap(100, 0,  logo24_battery100_bmp, 24, 16, WHITE);
  }
  else if (batteryLevel==101) {
    oled.drawBitmap(100, 0,  logo24_batteryCharging_bmp, 24, 16, WHITE);
  }
  else {
    Serial.print("Battery value invalid");
  }
}
